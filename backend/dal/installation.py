from backend.dal import client


def get_pumps_configuration():
    result = client.find("installation", {})
    return result


def set_drink_to_pump(drink_name,pump_number, location):
    return client.update("installation", {"pin": pump_number}, {"drink" : drink_name, "location": location})["ok"]


