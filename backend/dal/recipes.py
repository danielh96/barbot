from backend.dal import client
from backend.dal.installation import get_pumps_configuration


def get_all_available_recipes():
    result = client.find("recepies", {})
    installed_drinks = get_all_installed_drinks()
    available_recipes = []
    for recipe in result:
        is_good = True
        for drink in recipe["ingredients"].keys():
            if drink not in installed_drinks:
                is_good = False
                break
        if is_good:
            available_recipes.append(recipe)
    return available_recipes


def get_all_installed_drinks():
    pump_configurations = get_pumps_configuration()
    return [conf["drink"] for conf in pump_configurations]


def get_recipe(name):
    recipes = client.find("recepies", {"name": name})
    if len(recipes) != 1:
        raise Exception(f"Error while fetching {name}- found a number different than 1")
    return recipes[0]
