from backend.dal import client


def get_known_drinks():
    result = client.find("drinks", {})
    return result


def add_known_drink(name):
    return client.insert("drinks", {"name": str(name), "picture": None})
