import asyncio

from gpiozero import DigitalOutputDevice


class Pumps(object):
    def __init__(self, pins_list):
        self.device_mapping = {}
        for pin_num in pins_list:
            self.device_mapping[pin_num] = DigitalOutputDevice(pin_num)
            self.device_mapping[pin_num].on()

    async def clean_all(self):
        tasks = []
        for key in self.device_mapping.keys():
            tasks.append(asyncio.create_task(self.pump(key, 100)))

        for task in tasks:
            await task

    async def pump_cocktail(self,pumping_conf):
        tasks = []
        for pump_pin, amount in pumping_conf.items():
            tasks.append(asyncio.create_task(self.pump(pump_pin, amount)))

        for task in tasks:
            await task

    async def pump(self,pin_num, milliliter):
        print(f"on:{pin_num}")
        self.device_mapping[pin_num].off()
        await asyncio.sleep(milliliter*1

                            )
        self.device_mapping[pin_num].on()
        print(f"off:{pin_num}")

