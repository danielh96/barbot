from backend.dal.installation import get_pumps_configuration
from backend.dal.recipes import get_recipe
from backend.hal.pumps import Pumps


async def make_cocktail(name):
    # get data
    recipe = get_recipe(name)
    configuration = get_pumps_configuration()
    available_drinks = [installation["drink"] for installation in configuration]
    ingredients = recipe["ingredients"]

    # check availability
    for ing in ingredients.keys():
        if ing not in available_drinks:
            raise Exception(f"not all ingredients are available: {ing}")

    # distribute workload per pin
    pumping_conf = {}
    for ing in ingredients:
        matches = [installation for installation in configuration if installation["drink"] == ing]
        for match in matches:
            pumping_conf[match["pin"]] = ingredients[ing] / len(matches)

    # pump
    hal = Pumps([installation["pin"] for installation in configuration])
    await hal.pump_cocktail(pumping_conf)

    return pumping_conf

