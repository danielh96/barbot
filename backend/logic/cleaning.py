from backend.dal.installation import get_pumps_configuration
from backend.hal.pumps import Pumps


async def clean_all():
    configuration = get_pumps_configuration()
    # pump
    pins = [installation["pin"] for installation in configuration]
    hal = Pumps(pins)
    await hal.clean_all()

    return pins