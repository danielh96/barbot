import {Carousel} from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import "../../styles/main.css"; // requires a loader
import React from 'react';


const InstallationColumnStyle = {
    height: "inherit",
    flex: 1,
    display: "flex",
    flexDirection: "column"
};

class MainPage extends React.Component {
    componentWillMount() {
        this.setState({drinks : [{pic:"../cocktails/redhighglass.svg",name:"yum"}, {pic:"../cocktails/blue.svg",name:"yum2"}]});
        fetch("/getAllAvailableRecipes", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({})
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({drinks: result.data});

                },

                (error) => {
                    alert("failed to fetch drink from server" + error)
                }
            )

        fetch("/getServerIp", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({})
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({ip: result.data})

                },

                (error) => {
                    alert("failed to fetch ip from server")
                }
            )

    }

    constructor(props) {
        super(props);
        this.state = {ip: "unknown", drinks: [], selected_index: 0};


    }

    makeDrink() {
        console.info("making" + this.state.selected_index)
        debugger
        var drink_name = this.state.drinks[this.state.selected_index].name
        fetch("/makeDrink", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({

                "name": drink_name

            })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    debugger
                    if (result.status ==="error"){
                        alert("failed to make drink because" + result.data)
                    }else{
                        // done making drink

                    }

                },

                (error) => {
                    debugger
                    alert("failed to make drink because" + error)
                }
            )


    }

    handleChange(selected_index) {
        this.state.selected_index = selected_index
    }

    render() {
        return <div style={{
            height: "inherit",
            display: "flex",
            flex: "1",
            flexDirection: "row",
            minWidth: "100%"
        }}>


            <div style={InstallationColumnStyle}>

            </div>

            <div style={{
                height: "inherit",
                flex: 6,
                display: "flex",
                flexDirection: "column"
            }}>

                <div id={"title"} style={{flex: 1}}>
                    {this.state.ip}
                </div>
                <div id={"main-carousel"} style={{flex: 9}}>


                    <Carousel showThumbs={false}
                              infiniteLoop={true}
                              autoPlay={true}
                              centerMode={true}
                              dynamicHeight={true}
                              thumbWidth={"5%"}
                              onChange={(x) => this.handleChange(x)}>
                        {

                            this.state.drinks.map(image => {
                                return <div onClick={() => this.makeDrink()}>
                                    <img src={image.pic} style={{height: "80vh"}}/>
                                    <p className="legend">{image.name}</p>
                                </div>
                            })

                        }


                    </Carousel>

                </div>


            </div>
            <div style={InstallationColumnStyle}>

            </div>


        </div>;
    }
}

export {MainPage}