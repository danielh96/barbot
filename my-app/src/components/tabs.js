import {AppBar, Typography, withStyles} from "@material-ui/core";
import React from "react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import {Tab, Tabs} from "material-ui";
import {MainPage} from "./mainPage/mainPage";
import {AdminPage} from "./adminPage/adminPage";

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}

const styles = theme => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
});

class ScrollableTabsButtonAuto extends React.Component {
    state = {
        value: 0,
    };

    handleChange = (value,event) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;

        return (
            <div id={"X"}>
                <MuiThemeProvider>

                    <AppBar position="static" color="primary">
                        <Tabs
                            value={value}
                            onChange={this.handleChange}
                            indicatorColor="primary"
                            textColor="primary"
                            scrollable
                            scrollButtons="auto"
                        >
                            <Tab className={"tab"} label="Main Page" value={0}/>
                            <Tab className={"tab"} label="Configure" value={1} />

                        </Tabs>
                    </AppBar>
                    {value === 0 && <TabContainer>
                        <MainPage/>

                    </TabContainer>}

                    {value === 1 && <TabContainer>
                        <AdminPage/>
                    </TabContainer>}


                </MuiThemeProvider>
            </div>

        );
    }
}

export default withStyles(styles)(ScrollableTabsButtonAuto);
