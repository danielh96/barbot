import React from 'react';

class Installation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            installationName: props.name,
            disconnected: props.disconnected || true,
            pin: null,
            location: null
        };

    }
    clicked(){
        if (this.state.installationName == null){
            this.props.install_drink(this.state.location)
        }else{
            this.clear()
        }
    }
    update_server(drink_name){
        fetch("/setDrinkToPump", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({
                "pin": this.state.pin,
                "drink": drink_name,
                "location": this.state.location
            })
        })
            .then(res => res.json())
            .then(
                (result) => {

                    this.state.installationName = drink_name
                    this.setState(this.state)

                },

                (error) => {
                    alert("failed to fetch installtion status from server- page cannot load")
                }
            )

    }
    clear() {

        this.update_server(null)


    }

    render() {
        return <button className={"installation " + (this.state.disconnected && "disconnected")}
                       onClick={() => this.clicked()}>
            {this.props.location}
            <br/>
            {this.state.disconnected ? "Disconnected" : (this.state.installationName || "Not Installed")}
        </button>

    }
}

export {Installation}