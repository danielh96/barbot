import {Carousel} from 'react-responsive-carousel';
import {Installation} from "./installation";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import "../../styles/admin.css"; // requires a loader
import React from 'react';


const InstallationColumnStyle = {
    height: "inherit",
    flex: 1,
    display: "flex",
    flexDirection: "column"
};

class AdminPage extends React.Component {
    componentWillMount() {
        fetch("/getKnownDrinks", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({})
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.drinks_data = result.data
                    this.setState({drinks : result.data}, () => window.dispatchEvent(new Event('resize')));
                },

                (error) => {
                    alert("failed to fetch drink from server" + error)
                }
            )

    }
    constructor(props) {
        super(props);
        this.state = {ip: "unknown", drinks:[], selected_index:0};
        this.installations = new Array(10).fill(0).map(() => React.createRef())
        this.carousel = React.createRef();

        fetch("/getInstallations", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({})
        })
            .then(res => res.json())
            .then(
                (result) => {

                    for (let installation of result.data) {
                        this.installations[installation.location - 1].current.setState({
                            "installationName": installation.drink,
                            "pin": installation.pin,
                            "location": installation.location,
                            "disconnected": false
                        })

                    }

                },

                (error) => {
                    alert("failed to fetch installtion status from server- page cannot load")
                }
            )


        fetch("/getServerIp", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({})
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({ip: result.data})

                },

                (error) => {
                    alert("failed to fetch ip from server")
                }
            )




    }
    install_drink(location){
        debugger
        console.log(location)
        var installation = this.installations[location-1].current
        console.log(installation)
        var drink_name = this.state.drinks[this.state.selected_index].name
        console.log(drink_name)
        installation.update_server(drink_name)

    }
    clean(){
        fetch("/clean", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({})
        })
            .then(res => res.json())
            .then(
                (result) => {
                    if (result.status === "error"){
                        alert("failed to make drink because" + result.data)
                    }else{
                        // done cleaning

                    }
                },

                (error) => {
                    alert("failed to fetch ip from server")
                }
            )
    }
    clearAll() {

        for (let installation of this.installations) {
            installation.current.clear()
        }

    }

    handleChange(selected_index) {
        this.state.selected_index = selected_index
    }

    render() {
        return <div style={{
            height: "inherit",
            display: "flex",
            flex: "1",
            flexDirection: "row",
            minWidth: "100%"
        }}>


            <div style={InstallationColumnStyle}>
                <Installation ref={this.installations[0]} location={1} install_drink={(location)=>this.install_drink(location)}>test</Installation>
                <Installation ref={this.installations[1]} location={2} install_drink={(location)=>this.install_drink(location)}>test</Installation>
                <Installation ref={this.installations[2]} location={3} install_drink={(location)=>this.install_drink(location)}>test</Installation>
                <Installation ref={this.installations[3]} location={4} install_drink={(location)=>this.install_drink(location)}>test</Installation>
                <Installation ref={this.installations[4]} location={5} install_drink={(location)=>this.install_drink(location)}>test</Installation>

            </div>

            <div
            style={{
                height: "inherit",
                flex: 4,
                display: "flex",
                flexDirection: "column"
            }}>

                <div id={"title"} style={{flex: 1}}>
                    {this.state.ip}
                </div>
                <div ref={this.carousel} style={{flex: 3}}>


                    <Carousel id="carousel"
                              showThumbs={false}
                              infiniteLoop={true}
                              autoPlay={false}
                              centerMode={false}
                              dynamicHeight={true}
                              thumbWidth={"5%"}
                              onChange={(x)=>this.handleChange(x)}>
                        {

                            this.state.drinks.map(image => {
                                return <div>
                                    <img src={image.pic} style={{height: "60vh"}}/>
                                    <p className="legend">{image.name}</p>
                                </div>
                            })

                        }


                    </Carousel>

                </div>
                <div style={{flex: 1, display: "flex", justifyContent: "center"}}>
                    <button id={"clear"} className={"action"} onClick={() => this.clearAll()}>reset Installations</button>
                    <button id={"clean"} className={"action"} onClick={() => this.clean()}>Clean Pipes</button>

                </div>

            </div>
            <div style={InstallationColumnStyle}>
                <Installation ref={this.installations[5]} location={6} install_drink={(location)=>this.install_drink(location)}>test</Installation>
                <Installation ref={this.installations[6]} location={7} install_drink={(location)=>this.install_drink(location)}>test</Installation>
                <Installation ref={this.installations[7]} location={8} install_drink={(location)=>this.install_drink(location)}>test</Installation>
                <Installation ref={this.installations[8]} location={9} install_drink={(location)=>this.install_drink(location)}>test</Installation>
                <Installation ref={this.installations[9]} location={10} install_drink={(location)=>this.install_drink(location)}>test</Installation>

            </div>


        </div>;
    }
}

export {AdminPage}