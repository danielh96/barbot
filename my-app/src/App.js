import './App.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {MainPage} from "./components/mainPage/mainPage";
import {AdminPage} from "./components/adminPage/adminPage";
import {Tab, Tabs} from "material-ui";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import {AppBar, Paper, Typography} from "@material-ui/core";
import React from "react";
import ScrollableTabsButtonAuto from "./components/tabs";



function App() {
  return (
    <div className="App" style={{
        height: "inherit",
        display: "flex",
        flex: "1"
    }}>

        <ScrollableTabsButtonAuto/>

    </div>
  );
}

export default App;
